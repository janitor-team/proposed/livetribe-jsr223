#!/bin/sh -e

# called by uscan with '--upstream-version' <version> <file>

DIR=livetribe-jsr223-$2
TAG=v$2

svn export http://svn.codehaus.org/livetribe/garden/livetribe-jsr223/tags/$TAG $DIR

tar cfz livetribe-jsr223_$2.orig.tar.gz $DIR

rm -rf $DIR ../$TAG
